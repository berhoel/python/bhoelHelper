.. include:: ../README.rst

API Documentation
=================

.. currentmodule:: berhoel

.. autosummary::
   :nosignatures:

   helper
   helper.set_version
   helper.check_args
   helper.machar

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   _tmp/modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
