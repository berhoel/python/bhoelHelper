====================
 bhoelHelper module
====================

Some helper modules used in my other modules.

Installation
============

pip install bhoelHelper

Availability
============

The latest version should be available at my `GitLab
<https://gitlab.com/berhoel/python/bhoelHelper.git>`_ repository, the
package is avaliable at `pypi
<https://pypi.org/project/bhoelHelper/>`_ via ``pip install
bhoelHelper``.

Documentation
=============

Documentation for the project is avaliable `here <https://python.höllmanns.de/bhoelHelper/>`_.
